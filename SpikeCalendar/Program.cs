﻿using System;

namespace SpikeCalendar
{
    internal class Program
    {
        private static readonly User Migue = new User
        {
            CalendarId = "nkh96j6th0tduak8vpkt1ns9ho@group.calendar.google.com"
        };
        private static readonly User Dani = new User
        {
            CalendarId = "ki8rpa1qpdnip4tlabr60hra1c@group.calendar.google.com"
        };
        private static readonly User Jordi = new User
        {
            CalendarId = "codesai.com_5apq6bqdsv02f9sp9c804ttf0o@group.calendar.google.com"
        };
        
        private static void Main(string[] args)
        {
//            new Calendar().CreateEvent(Migue);
//            new Calendar().GetUpcomingEvents(Migue);
//            new Calendar().RemoveFirstUpcomingEvent(Dani);
//            new Calendar().PostponeFirstUpcomingEvent(Dani);
//            new Calendar().ClearAllEvents(Dani);
            //new Calendar().CheckConflictsOnCreatingEvent(Migue);
//            new Calendar().CheckConflictsInAllCalendarsOnCreatingEvent(Migue);
            var dateTime = DateTime.Today.AddDays(-1);
            new Calendar().GetAllEventsForAConcreteDay(Migue, dateTime, dateTime.AddHours(12));
        }
    }
}
