﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace SpikeCalendar
{
    public class Calendar 
    {
        public CalendarService CalendarService(User currentUser)
        {
            string[] scopes = { Google.Apis.Calendar.v3.CalendarService.Scope.Calendar };
            const string applicationName = "Spike Calendar";
            

            var clientSecretStream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read);

            var userCredentials = /*await?*/ GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(clientSecretStream).Secrets,
                scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../credentials/" + currentUser.CalendarId), true)).Result;

            var calendarService = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = userCredentials,
                ApplicationName = applicationName,

            });

            return calendarService;

        }

        public void CreateEvent(User user)
        {
            var swimEvent = new Event
            {
                Summary = "Swin again",
                Location = "Al lao de casa",
                Description = "Muchas piscinas muy piscinas",
                Start = new EventDateTime
                {
                    DateTime = DateTime.UtcNow.AddDays(3),
                    TimeZone = "Europe/London"
                },
                End = new EventDateTime
                {
                    DateTime = DateTime.UtcNow.AddDays(3).AddHours(1),
                    TimeZone = "Europe/London"
                }
            };

            var request = CalendarService(user).Events.Insert(swimEvent, user.CalendarId);
            request.Execute();
        }

        public void CreateConcreteEvent(User user, Event @event)
        {
            var request = CalendarService(user).Events.Insert(@event, user.CalendarId);
            request.Execute();
        }

        public Events GetUpcomingEvents(User currentUser)
        {
            var request = CalendarService(currentUser).Events.List(currentUser.CalendarId);
            request.TimeMin = DateTime.Now;
            request.ShowDeleted = false;
            request.SingleEvents = true;
//            request.Q = "swim";
            request.MaxResults = 10;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            var events = request.Execute();
            Console.WriteLine("Upcoming events:");
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    var when = eventItem.Start.DateTime.ToString();
                    if (string.IsNullOrEmpty(when))
                    {
                        when = eventItem.Start.Date;
                    }
                    Console.WriteLine("{0} ({1})", eventItem.Summary, when);
                }
            }
            else
            {
                Console.WriteLine("No upcoming events found.");
            }
            return events;
        }

        public void RemoveFirstUpcomingEvent(User user)
        {
            var events = GetUpcomingEvents(user);

            events.Items.First(@event =>
            {
                CalendarService(user).Events.Delete(user.CalendarId, @event.Id).Execute();
                return true;
            });
            
            Console.WriteLine("---------------------------------------");
            GetUpcomingEvents(user);
        }

        public void PostponeFirstUpcomingEvent(User user)
        {
            var events = GetUpcomingEvents(user);

            events.Items.First(upcomingEvent =>
            {
                upcomingEvent.Start = new EventDateTime {DateTime = upcomingEvent.Start.DateTime.Value.AddDays(1)};
                upcomingEvent.End = new EventDateTime {DateTime = upcomingEvent.End.DateTime.Value.AddDays(1)};

                CalendarService(user).Events.Update(upcomingEvent, user.CalendarId, upcomingEvent.Id).Execute();
                return true;
            });

            Console.WriteLine("---------------------------------------");
            GetUpcomingEvents(user);
        }

        public void ClearAllEvents(User user)
        {
            var events = GetAllEvents(user);
            foreach (var @event in events.Items)
            {
                CalendarService(user).Events.Delete(user.CalendarId, @event.Id).Execute();
            }
            Console.WriteLine("---------------------");
            GetAllEvents(user);
        }

        public Events GetAllEvents(User currentUser)
        {
            var maxResults = 2500;
            var request = CalendarService(currentUser).Events.List(currentUser.CalendarId);
            request.TimeMin = DateTime.MinValue;
            request.TimeMax = DateTime.MaxValue;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = maxResults;

            var events = request.Execute();
            printEvents(events);
            return events;
        }

        public Events GetAllEventsForAConcreteDay(User currentUser, DateTime start, DateTime end)
        {
            var maxResults = 2500;
            var request = CalendarService(currentUser).Events.List(currentUser.CalendarId);
            request.TimeMin = start;
            request.TimeMax = end;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = maxResults;

            var events = request.Execute();
            printEvents(events);
            return events;
        }

        private static void printEvents(Events events)
        {
            Console.WriteLine("Upcoming events:");
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    var when = eventItem.Start.DateTime.ToString();
                    if (string.IsNullOrEmpty(when))
                    {
                        when = eventItem.Start.Date;
                    }
                    Console.WriteLine("{0} ({1})", eventItem.Summary, when);
                }
            }
            else
            {
                Console.WriteLine("No upcoming events found.");
            }
        }

        public void CheckConflictsOnCreatingEvent(User user)
        {
            var startDateTime = new DateTime(2016, 6, 30, 13, 59, 0);
            var endDateTime = new DateTime(2016, 6, 30, 15, 59, 0);
            var @event = new Event
            {
                Summary = "Conflictable event",
                Location = "Hello its me",
                Description = "I was wondering",
                Start = new EventDateTime
                {
                    DateTime = startDateTime,
                    TimeZone = "Europe/London"
                },
                End = new EventDateTime
                {
                    DateTime = endDateTime,
                    TimeZone = "Europe/London"
                }
            };
            CreateConcreteEvent(user, @event);

            var calendarId = user.CalendarId;
            checkConflictInCalendar(user, calendarId, startDateTime, endDateTime);
        }

        private void checkConflictInCalendar(User user, string calendarId, DateTime startDateTime, DateTime endDateTime)
        {
            var request = CalendarService(user).Events.List(calendarId);
            request.TimeMin = startDateTime;
            request.TimeMax = endDateTime;
            var eventsInTimeRange = request.Execute();

            if (eventsInTimeRange.Items.Count == 0)
            {
                Console.WriteLine("No hay eventos en esta franja horaria");
            }
            else
            {
                printEvents(eventsInTimeRange);
            }
        }

        public void CheckConflictsInAllCalendarsOnCreatingEvent(User user)
        {
            // create specific Event in a Calendar
            var startDateTime = new DateTime(2016, 8, 1, 13, 59, 0);
            var endDateTime = new DateTime(2016, 8, 1, 15, 59, 0);
            var @event = new Event
            {
                Summary = "Test event",
                Start = new EventDateTime
                {
                    DateTime = startDateTime,
                    TimeZone = "Europe/London"
                },
                End = new EventDateTime
                {
                    DateTime = endDateTime,
                    TimeZone = "Europe/London"
                }
            };
            CreateConcreteEvent(user, @event);

            // check in all calendars
            var calendars = CalendarService(user).CalendarList.List().Execute();
            foreach (var calendar in calendars.Items)
            {
                checkConflictInCalendar(user, calendar.Id, startDateTime, endDateTime);
            }
        }
    }
}